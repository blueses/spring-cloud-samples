package config.zk16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * zk 配置中心
 */
@SpringBootApplication
public class ConfigZk16Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigZk16Application.class, args);
	}
}
