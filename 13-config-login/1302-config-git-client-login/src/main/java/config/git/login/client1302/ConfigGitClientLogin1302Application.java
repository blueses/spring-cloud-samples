package config.git.login.client1302;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 客户端
 */
@SpringBootApplication
public class ConfigGitClientLogin1302Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitClientLogin1302Application.class, args);
	}
}
