package config.git.login.server1301;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigGitServerLogin1301Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitServerLogin1301Application.class, args);
	}
}
