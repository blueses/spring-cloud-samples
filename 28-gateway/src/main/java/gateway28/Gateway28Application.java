package gateway28;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 启动类
 */
@SpringBootApplication
public class Gateway28Application {

	public static void main(String[] args) {
		SpringApplication.run(Gateway28Application.class, args);
	}
}
