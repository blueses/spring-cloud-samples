package gateway.route29;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 启动类
 */
@SpringBootApplication
public class GatewayRoute29Application {

	public static void main(String[] args) {
		SpringApplication.run(GatewayRoute29Application.class, args);
	}
}
