package config.jdbc.server1101;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigJdbcServer1101Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigJdbcServer1101Application.class, args);
	}
}
