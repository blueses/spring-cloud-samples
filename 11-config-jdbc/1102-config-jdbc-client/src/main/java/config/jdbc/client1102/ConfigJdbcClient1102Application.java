package config.jdbc.client1102;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置 客户端
 */
@SpringBootApplication
public class ConfigJdbcClient1102Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigJdbcClient1102Application.class, args);
	}
}
