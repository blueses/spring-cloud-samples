package config.jdbc.client1102;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${name.a.b}")
    private String name;

    @GetMapping("hello")
    public String hello(){
        return name;
    }
}
