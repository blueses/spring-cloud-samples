package hystrix.over.time20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class HystrixOverTime20Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixOverTime20Application.class, args);
    }

}
