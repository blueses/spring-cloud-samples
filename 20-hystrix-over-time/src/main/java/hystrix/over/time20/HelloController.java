package hystrix.over.time20;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	private static final Logger logger = LoggerFactory.getLogger(HelloController.class);


	/** 设置超时时间为3000毫秒，sleep时间为4000毫秒，必定发生超时 改为降级调用helloHystrix方法*/
	@HystrixCommand(
			commandProperties = {
				@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
			},
			fallbackMethod = "helloHystrix"
	)
	@GetMapping("hello")
	public String hello(){
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			logger.error("超时！降级！");
		}
		return "hello 20";
	}

	public String helloHystrix(){
		return "hello 20 hystrix over time";
	}
}
