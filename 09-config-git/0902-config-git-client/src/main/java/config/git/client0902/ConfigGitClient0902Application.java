package config.git.client0902;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置git 客户端
 */
@SpringBootApplication
public class ConfigGitClient0902Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitClient0902Application.class, args);
	}
}
