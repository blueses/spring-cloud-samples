package config.git.server0901;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigGitServer0901Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitServer0901Application.class, args);
	}
}
