package ribbon.produce1801;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RibbonProduce1801Application {

    public static void main(String[] args) {
        SpringApplication.run(RibbonProduce1801Application.class, args);
    }

}
