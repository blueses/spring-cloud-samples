package ribbon.consume1803;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GetHelloController {


	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 多次访问可以查看到负载均衡的效果
	 *
	 * 要使用服务名作为host，需要在RestTemplate定义的方法上面加上 @LoadBalanced 注解
	 */
	@GetMapping("get")
	public String hello(){
		return restTemplate.getForObject("http://ribbon-api-produce/hello", String.class);
	}

}
