package ribbon.produce1802;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RibbonProduce1802Application {

    public static void main(String[] args) {
        SpringApplication.run(RibbonProduce1802Application.class, args);
    }

}
