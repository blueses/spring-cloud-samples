package hystrix.limit21;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloController {

	@Autowired
	private RestTemplate restTemplate;


	/**
	 * coreSize （线程池coreSize 默认值10）
	 */
	@HystrixCommand(
			threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "1")
			},
			fallbackMethod = "helloHystrix"
	)
	@GetMapping("hello")
	public String hello(){
		return "hello 21";
	}

	public String helloHystrix(){
		return "hello 21 hystrix limit";
	}





	/* 同时访问多个，只有一个成功，其它都失败，降级 */
	@GetMapping("get")
	public String getHello(){
		new Thread(() -> System.out.println(restTemplate.getForObject("http://localhost:8021/hello",String.class))).start();
		new Thread(() -> System.out.println(restTemplate.getForObject("http://localhost:8021/hello",String.class))).start();
		new Thread(() -> System.out.println(restTemplate.getForObject("http://localhost:8021/hello",String.class))).start();
		new Thread(() -> System.out.println(restTemplate.getForObject("http://localhost:8021/hello",String.class))).start();
		return "hello 21 get";
	}

}
