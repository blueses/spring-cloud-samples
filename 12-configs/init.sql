/*初始化sql*/


CREATE TABLE `spring_cloud_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application` varchar(255) DEFAULT NULL,
  `profile` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;



INSERT INTO `spring_cloud_config` VALUES ('1', 'app', 'dev', 'trunk', 'name.a.b', 'jdbc-trunk-dev');