package configs.server1201;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigServers1201Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServers1201Application.class, args);
	}
}
