package configs.client1202;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置 客户端
 */
@SpringBootApplication
public class ConfigsClient1202Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigsClient1202Application.class, args);
	}
}
