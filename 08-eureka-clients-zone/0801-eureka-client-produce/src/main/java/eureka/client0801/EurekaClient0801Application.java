package eureka.client0801;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaClient0801Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient0801Application.class, args);
    }

}
