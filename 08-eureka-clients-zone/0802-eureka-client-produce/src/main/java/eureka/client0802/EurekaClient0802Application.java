package eureka.client0802;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaClient0802Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient0802Application.class, args);
    }

}
