package eureka.client0803;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GetHelloController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private LoadBalancerClient loadBalancer;

	/** 多次访问可以查看到负载均衡的效果 */
	@GetMapping("get")
	public String hello(){
		ServiceInstance instance = loadBalancer.choose("eureka-client-zone-produce");
		String base = String.format("http://%s:%s", instance.getHost(), instance.getPort());
		return restTemplate.getForObject(base + "hello",String.class);
	}

}
