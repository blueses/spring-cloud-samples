package sleuth.client2702;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 启动类
 */
@EnableFeignClients
@SpringBootApplication
public class SleuthClient2702Application {

	public static void main(String[] args) {
		SpringApplication.run(SleuthClient2702Application.class, args);
	}
}
