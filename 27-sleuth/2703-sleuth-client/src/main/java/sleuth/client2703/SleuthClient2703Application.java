package sleuth.client2703;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 启动类
 */
@EnableFeignClients
@SpringBootApplication
public class SleuthClient2703Application {

	public static void main(String[] args) {
		SpringApplication.run(SleuthClient2703Application.class, args);
	}
}
