package sleuth.client2703;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetHelloController {

    @Autowired(required = false)
    private GetHelloService getHelloService;

    @GetMapping("get")
    public String get(){
        return getHelloService.hello();
    }
}
