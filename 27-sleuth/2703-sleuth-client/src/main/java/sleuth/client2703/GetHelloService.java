package sleuth.client2703;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("sleuth-client-2702")
public interface GetHelloService {

    @GetMapping("hello")
    String hello();

}
