package hystrix.turbine.hystrix2302;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	/** 必定发生异常 产生熔断 改为降级调用helloHystrix方法*/
	@HystrixCommand(fallbackMethod = "helloHystrix")
	@GetMapping("hello")
	public String hello(){
		int a = 1/0;
		return "hello 2302";
	}

	public String helloHystrix(){
		return "hello 2302 hystrix exception";
	}
}
