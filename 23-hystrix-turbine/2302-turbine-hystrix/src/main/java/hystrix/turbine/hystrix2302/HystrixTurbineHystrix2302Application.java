package hystrix.turbine.hystrix2302;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class HystrixTurbineHystrix2302Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixTurbineHystrix2302Application.class, args);
    }


}
