package hystrix.turbine.hystrix2303;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class HystrixTurbineHystrix2303Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixTurbineHystrix2303Application.class, args);
    }


}
