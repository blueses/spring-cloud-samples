package hystrix.dashboard.trubine2301;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableTurbine
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixDashboardTurbine2301Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardTurbine2301Application.class, args);
    }


}
