package config.git.client2602;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 类上面加上 @RefreshScope 注解，在git仓库中的值发生变化后访问下面的链接：
 *
 * http://localhost:8262/actuator/bus-refresh
 *
 * 就能做到自动刷新配置
 */
@RefreshScope
@RestController
public class HelloController {

    @Value("${name.a.b}")
    private String name;

    @GetMapping("hello")
    public String hello(){
        return name;
    }
}
