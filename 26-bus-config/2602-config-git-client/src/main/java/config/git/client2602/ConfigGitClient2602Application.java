package config.git.client2602;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置git 客户端
 */
@SpringBootApplication
public class ConfigGitClient2602Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitClient2602Application.class, args);
	}
}
