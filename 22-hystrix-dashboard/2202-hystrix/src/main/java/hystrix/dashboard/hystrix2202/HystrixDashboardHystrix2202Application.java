package hystrix.dashboard.hystrix2202;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class HystrixDashboardHystrix2202Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardHystrix2202Application.class, args);
    }


}
