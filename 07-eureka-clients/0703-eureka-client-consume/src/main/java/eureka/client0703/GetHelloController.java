package eureka.client0703;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class GetHelloController {

	@Autowired
	private EurekaClient eurekaClient;

	@Autowired
	private RestTemplate restTemplate;

	/** 多次访问可以查看到负载均衡的效果 */
	@GetMapping("get")
	public String hello(){
		InstanceInfo instance = eurekaClient.getNextServerFromEureka("eureka-client-produce", false);
		String baseUrl = instance.getHomePageUrl();
		String hello = restTemplate.getForObject(baseUrl+"hello",String.class);
		return hello;
	}

	@Autowired
	private DiscoveryClient discoveryClient;

	@GetMapping("gets")
	public Object gets(){
		List<ServiceInstance> list = discoveryClient.getInstances("eureka-client-produce");
		return list;
	}

}
