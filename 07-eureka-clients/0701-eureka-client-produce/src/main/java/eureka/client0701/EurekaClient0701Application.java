package eureka.client0701;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaClient0701Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient0701Application.class, args);
    }

}
