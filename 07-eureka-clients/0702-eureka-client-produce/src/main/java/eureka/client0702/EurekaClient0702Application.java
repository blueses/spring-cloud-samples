package eureka.client0702;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaClient0702Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient0702Application.class, args);
    }

}
