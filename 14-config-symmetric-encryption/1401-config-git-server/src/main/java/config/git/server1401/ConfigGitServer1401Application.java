package config.git.server1401;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigGitServer1401Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitServer1401Application.class, args);
	}
}
