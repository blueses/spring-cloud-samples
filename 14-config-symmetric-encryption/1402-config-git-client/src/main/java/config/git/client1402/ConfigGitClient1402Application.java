package config.git.client1402;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置git 客户端
 */
@SpringBootApplication
public class ConfigGitClient1402Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigGitClient1402Application.class, args);
	}
}
