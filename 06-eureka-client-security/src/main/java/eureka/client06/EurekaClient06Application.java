package eureka.client06;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaClient06Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient06Application.class, args);
    }

}
