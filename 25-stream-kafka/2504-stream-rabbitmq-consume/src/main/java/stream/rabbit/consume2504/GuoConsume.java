package stream.rabbit.consume2504;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(GuoSink.class)
public class GuoConsume {
    private static final Logger logger = LoggerFactory.getLogger(GuoConsume.class);

    @StreamListener(GuoSink.INPUT)
    public void receive(String msg) {
        logger.info("guo 收到消息：{}", msg);
    }
}
