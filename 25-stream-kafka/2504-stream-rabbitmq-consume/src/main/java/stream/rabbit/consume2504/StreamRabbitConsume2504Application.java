package stream.rabbit.consume2504;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamRabbitConsume2504Application {

    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitConsume2504Application.class, args);
    }


}
