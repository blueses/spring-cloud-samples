package stream.rabbit.consume2504;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface GuoSink {
    String INPUT = Topic.TOPIC_GUO;

    @Input(INPUT)
    SubscribableChannel input();
}
