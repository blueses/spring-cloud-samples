package stream.kafka.produce2501;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

@EnableBinding(GuoSource.class)
public class GuoProduce {

    @Autowired
    private GuoSource guoSource;

    public void send(String msg) {
        guoSource.output().send(MessageBuilder.withPayload(msg).build());
    }

}
