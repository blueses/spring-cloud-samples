package stream.kafka.produce2501;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GuoSendController {

    @Autowired
    private GuoProduce guoProduce;

    @GetMapping("guo/send/{msg}")
    public void send(@PathVariable String msg){
        guoProduce.send(msg);
    }
}
