package stream.kafka.produce2501;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamKafkaProduce2501Application {

    public static void main(String[] args) {
        SpringApplication.run(StreamKafkaProduce2501Application.class, args);
    }


}
