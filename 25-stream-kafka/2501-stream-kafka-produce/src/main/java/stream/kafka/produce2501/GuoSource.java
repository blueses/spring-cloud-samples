package stream.kafka.produce2501;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface GuoSource {
    String OUTPUT = Topic.TOPIC_GUO;

    @Output(GuoSource.OUTPUT)
    MessageChannel output();
}
