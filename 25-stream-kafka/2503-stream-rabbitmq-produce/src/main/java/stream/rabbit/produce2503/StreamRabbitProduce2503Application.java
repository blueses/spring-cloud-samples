package stream.rabbit.produce2503;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamRabbitProduce2503Application {

    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitProduce2503Application.class, args);
    }


}
