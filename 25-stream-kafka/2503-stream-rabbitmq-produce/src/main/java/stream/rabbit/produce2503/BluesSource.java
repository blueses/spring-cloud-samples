package stream.rabbit.produce2503;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface BluesSource {

    String OUTPUT = Topic.TOPIC_BLUES;

    @Output(BluesSource.OUTPUT)
    MessageChannel output();
}
