package stream.rabbit.produce2503;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

@EnableBinding(BluesSource.class)
public class BluesProduce {

    @Autowired
    private BluesSource bluesSource;

    public void send(String msg) {
        bluesSource.output().send(MessageBuilder.withPayload(msg).build());
    }

}
