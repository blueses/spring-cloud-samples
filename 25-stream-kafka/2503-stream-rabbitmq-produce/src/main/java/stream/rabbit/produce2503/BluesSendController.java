package stream.rabbit.produce2503;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BluesSendController {

    @Autowired
    private BluesProduce bluesProduce;

    @GetMapping("blues/send/{msg}")
    public void send(@PathVariable String msg){
        bluesProduce.send(msg);
    }
}
