package stream.kafka.consume2502;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamKafkaConsume2502Application {

    public static void main(String[] args) {
        SpringApplication.run(StreamKafkaConsume2502Application.class, args);
    }


}
