package stream.kafka.consume2502;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BluesSink.class)
public class BluesConsume {
    private static final Logger logger = LoggerFactory.getLogger(BluesConsume.class);

    @StreamListener(BluesSink.INPUT)
    public void receive(String msg) {
        logger.info("blues 收到消息：{}", msg);
    }
}
