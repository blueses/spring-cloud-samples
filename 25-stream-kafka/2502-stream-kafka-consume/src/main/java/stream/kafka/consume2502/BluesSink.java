package stream.kafka.consume2502;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface BluesSink {
    String INPUT = Topic.TOPIC_BLUES;

    @Input(INPUT)
    SubscribableChannel input();
}
