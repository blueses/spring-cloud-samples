package config.svn.server1001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 配置中心启动类
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigSvnServer1001Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigSvnServer1001Application.class, args);
	}
}
