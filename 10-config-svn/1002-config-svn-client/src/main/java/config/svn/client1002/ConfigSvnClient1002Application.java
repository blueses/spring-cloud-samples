package config.svn.client1002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 配置git 客户端
 */
@SpringBootApplication
public class ConfigSvnClient1002Application {

	public static void main(String[] args) {
		SpringApplication.run(ConfigSvnClient1002Application.class, args);
	}
}
