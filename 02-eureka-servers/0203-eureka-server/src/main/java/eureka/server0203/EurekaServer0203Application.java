package eureka.server0203;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServer0203Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer0203Application.class, args);
    }

}
