package eureka.server0201;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServer0201Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer0201Application.class, args);
    }

}
