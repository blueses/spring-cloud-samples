package hystrix.fuse19;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 可以针对单个方法进行熔断降级
 */
@RestController
public class HelloController {


	/** 必定发生异常 产生熔断 改为降级调用helloHystrix方法*/
	@HystrixCommand(fallbackMethod = "helloHystrix")
	@GetMapping("hello")
	public String hello(){
		int a = 1/0;
		return "hello 19";
	}

	public String helloHystrix(){
		return "hello 19 hystrix exception";
	}

	/** 必定发生异常 产生熔断 改为降级调用getHystrix方法*/
	@HystrixCommand(fallbackMethod = "getHystrix")
	@GetMapping("get")
	public String get(){
		int a = 1/0;
		return "get 19";
	}

	public String getHystrix(){
		return "get 19 hystrix";
	}
}
