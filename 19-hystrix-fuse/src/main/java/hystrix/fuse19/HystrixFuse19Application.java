package hystrix.fuse19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class HystrixFuse19Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixFuse19Application.class, args);
    }

}
