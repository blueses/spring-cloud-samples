package feign.client2402;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class GetController {

	@Autowired(required = false)
	private HelloService helloService;

	@GetMapping("get")
	public String get(){
		Map map = new HashMap(2);
		map.put("name","blues");
		return "get " + helloService.hello(map);
	}

}
