package feign.client2402;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient("feign-server")
public interface HelloService {

    @GetMapping("hello")
    String hello(@SpringQueryMap Map map);

}
