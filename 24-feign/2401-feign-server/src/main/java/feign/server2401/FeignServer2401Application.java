package feign.server2401;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class FeignServer2401Application {

    public static void main(String[] args) {
        SpringApplication.run(FeignServer2401Application.class, args);
    }


}
