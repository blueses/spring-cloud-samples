package eureka.server0302;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServer0302Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer0302Application.class, args);
    }

}
